// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    sourceType: 'module',
    parser: '@babel/eslint-parser'
  },
  env: {
    browser: true,
    // 'cypress/globals': true
  },
  extends: [
    'plugin:vue/strongly-recommended',
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue',
    'vuetify'
    // 'cypress'
  ],
  "rules": {
    'vue/multi-word-component-names': "off",
    "vue/max-attributes-per-line": "off",
    'vue/html-indent': 'off',
    'vuetify/no-deprecated-classes': 'error',
    'vuetify/grid-unknown-attributes': 'error',
    'vuetify/no-legacy-grid': 'error',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'template-curly-spacing' : 'off',
    'indent' : 'off'
  }
}
