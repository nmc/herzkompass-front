FROM node:alpine
EXPOSE 8080

RUN mkdir -p /opt/src

WORKDIR /opt/src

ADD . ./

RUN npm install --production
RUN rm -rf dist
RUN ./node_modules/webpack-cli/bin/cli.js --config build/webpack.config.prod.js
USER 1001
CMD [ "node", "prod-server.js"]
