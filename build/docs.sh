docker run \
-v $(pwd)/doc:/opt/doc \
doc:latest bash -c \
"pandoc -f markdown -t latex -s -o doc/documentation.pdf doc/introduction.md doc/user_guide.md doc/expert_guide.md doc/admin_guide.md"
