{
  description = "A very basic flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  };
  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
        inherit system;
        overlays = [];
    };
    tuntapName = "tapHeartapp";
    pythonPackages = pkgs.python3Packages;
    system = "x86_64-linux";
    qemuAdr = "10.13.12.27";
    appuioLogin = (pkgs.writeShellScriptBin "appuioLogin" ''
      oc login https://api.cloudscale-lpg-2.appuio.cloud:6443
    '');
    qemuConfig = { pkgs, config, lib, ... }: {
      imports = [
        "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix"
      ];
      # virtualisation.diskImage = "${target}.qcow2";
      # virtualisation.qemu.options = ["-drive file=${qcow2Drive}"];
      environment.systemPackages = with pkgs; [
        docker-compose
        vim
        qemu
        openshift
      ];
      networking.defaultGateway = {
        # address = "10.0.2.1";
        address = "10.13.12.1";
        interface = "eth0";
      };
      networking.firewall.enable = false;
      networking.nameservers = [ "10.0.0.1" "208.67.222.222" ];
      networking.useDHCP = false;
      users.users.nmc-schnauz = {
        isNormalUser = true;
        createHome = true;
        home = "/home/nmc-schnauz";
        password = "bling";
        uid = 1000;
        extraGroups = [ "wheel" "docker" ];
        shell = pkgs.zsh;
      };
      security.sudo = {
        enable = true;
        wheelNeedsPassword = false;
      };
      services.sshd.enable = true;
      virtualisation.msize = 8193;
      virtualisation.memorySize = 8193;
      virtualisation.diskSize = 4196;
      virtualisation.docker.enable = true;
      virtualisation.qemu = {
        networkingOptions = [
          "-nic tap,ifname=${tuntapName},script=no,downscript=no,model=virtio-net-pci"
        ];
        options = [
          "-display none"
          # "-nographic"
        ];
      };
    };
  in rec {
    qemu = import "${nixpkgs}/nixos" {
      inherit system;
      configuration = {...}: {
        imports = [qemuConfig];
        environment.systemPackages = [
          appuioLogin
          (pkgs.writeShellScriptBin "appuioLoginDocker" ''
            echo $(oc sa get-token gitlab-ci) | docker login -u serviceaccount --password-stdin registry.cloudscale-lpg-2.appuio.cloud
          '')
        ];
        networking.interfaces = {
          eth0.useDHCP = false;
          eth0.ipv4.addresses = [{
            address = qemuAdr;
            prefixLength = 24;
          }];
        };
      };
    };
    defaultPackage.x86_64-linux = qemu.config.system.build.vm;
    devShell.x86_64-linux = pkgs.mkShell {
      buildInputs = [
        pkgs.openshift
        appuioLogin
        pythonPackages.python
        pkgs.nodePackages.npm
        pkgs.nodejs
      ];
      shellHook = ''
        export PS1='\u@herz-front \$ '
      '';
    };
  };
}
