// var requestLog = 'request.log'
// const fs = require('fs')

describe('page', () => {
  beforeEach(() => {
    // cy.visit(':8080/')
    cy.clearLocalStorage()

    //   cy.clearCookies()
    // das hilft gegen Service Worker
    // registry.appuio.ch/unibas-app1/herzkompass-front
    // return window.caches.keys().then(function (cacheNames) {
    //   return Promise.all(
    //     cacheNames.map(function (cacheName) {
    //       return window.caches.delete(cacheName)
    //     })
    //   )
    // })
  })

  // var asyncWrite = function (fn, content) {
  //   return new Promise(resolve => {
  //     cy.writeFile(fn, content)
  //     resolve()
  //   })
  // }
  it('backend is available', () => {
    cy.visit(Cypress.env('backend'))
    cy.title().should('eq', 'Api Root – Django REST framework')
  })

  it('login to backend directly', () => {
    cy.visit(Cypress.env('backend'))
    cy.request('POST', 'rest-auth/login/', {
      email: 'daniel@dimmler.ch',
      password: 'DanielDimmler'
    })
      .then((response) => {
        // response.body is automatically serialized into JSON
        expect(response.body).to.have.property('key') // true
      })
  })
  // it('title is HeartApp', () => {
  //   cy.visit(Cypress.env('frontend'))
  //   cy.title().should('eq', 'HeartApp')
  // })
  // it('should log in', () => {
  //   cy.spy(window.console, 'log')
  //   // cy.clearCache
  //   cy.clearLocalStorage()
  //   cy.server()
  //   // This is the post call we are interested in capturing
  //   cy.route({
  //     method: 'POST',
  //     url: '**/rest-auth/login/**'
  //     // onRequest: (xhr) => {
  //     //   return asyncWrite('cypress/loginRequest.json', JSON.stringify(xhr))
  //     // },
  //     // onResponse: (xhr) => {
  //     //   return asyncWrite('cypress/loginResponse.json', JSON.stringify(xhr))
  //     // }
  //   }).as('login')

  //   cy.visit(Cypress.env('frontend'))

  //   cy.get('input#login-username').type('daniel@dimmler.ch')
  //   cy.get('input#login-password').type('DanielDimmler')

  //   cy.get('div#login form').submit().then((response) => {
  //   })

  //   cy.wait('@login').then(function (xhr) {
  //     expect(xhr.requestHeaders).to.have.property('Content-Type')
  //     expect(xhr.status).to.eq(200)
  //     expect(xhr.responseBody).to.have.property('key')
  //   })

  //   cy.url().should('include', '/profile')
  // })
})
