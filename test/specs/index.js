/* eslint-env mocha */
/* global $, $$, browser */
'use strict'
const assert = require('assert')
var config = require('../wdio.conf.js').config

/* Helpers */

let login = (usrname, psw) => {
  var username = $('input#login-username')
  username.waitForExist()
  username.setValue(usrname)
  var password = $('input#login-password')
  password.waitForExist()
  password.setValue(psw)
  $('#login form button[type="submit"]').click()
  browser.waitForExist('.toolbar__title')
}

let logout = () => {
  browser.url('/#/logout')
  browser.waitForExist('#login #login-username')
}

describe('Entry point', () => {
  it('should have a title', () => {
    browser.url('/')
    browser.localStorage('DELETE')
    browser.setViewportSize({
      width: 1280,
      height: 720
    })
    assert.strictEqualqual(browser.getTitle(), 'HeartApp')
  })
})

describe('User management', () => {
  describe('User signup', () => {
    // TODO
  })

  describe('User login', () => {
    it('should be possible', () => {
      login('albert@amann.ch', 'AlbertAmann')
      browser.waitForExist('.toolbar__content span')
      assert.strictEqual($('.toolbar__content span').getText(), 'albert@amann.ch')
    })
  })

  describe('User logout', () => {
    it('should be possible', () => {
      logout()
      browser.waitForExist('#login #login-username', 10000)
      assert.strictEqual(browser.getUrl(), 'http://localhost:8080/#/login')
    })
  })
})

describe('User workflow', () => {
  before(() => {
    login('albert@amann.ch', 'AlbertAmann')
  })
  // describe('User profile', () => {
  //   describe('view', () => {
  //     it('', () => {
  //       browser.url('/#/')
  //       browser.waitForExist('.toolbar__title')
  //     })
  //   })
  //   describe('edit', () => {
  //     it('', () => {
  //       browser.url('/#/edit')
  //       browser.waitForExist('.toolbar__title')
  //     })
  //   })
  // })
  // describe('Medical info', () => {
  //   describe('view', () => {
  //     it('', () => {
  //       browser.url('/#/medical-info')
  //       browser.waitForExist('.toolbar__title')
  //     })
  //   })
  //   describe('edit', () => {
  //     it('', () => {
  //       browser.url('/#/medical-info/edit')
  //       browser.waitForExist('.toolbar__title')
  //     })
  //   })
  // })
  describe('Heart disease', () => {
    describe('view', () => {
      // it('', () => {
      //   browser.url('/#/heart-diagnosis')
      //   browser.waitForExist('.toolbar__title')
      // })
    })
    describe('edit', () => {
      it('should be possible to select a category', () => {
        browser.url('/#/heart-diagnosis/edit')
        browser.waitForExist('.toolbar__title')

        let input = $('#category-selector .input-group')
        input.waitForVisible(10000)
        input.click()

        let select = $('.menu__content--select.menuable__content__active')
        select.waitForVisible(10000)

        assert.strictEqual(select.$('.list__tile__title').getText(), 'Normal anatomy')
      })
      it('should be possible to select a heart disease', () => {
        browser.url('/#/heart-diagnosis/edit')
        browser.waitForVisible('.toolbar__title')

        browser.pause(2000)

        let input2 = $('#heart-disease-selector .input-group')
        input2.waitForVisible(10000)
        input2.click()

        let select2 = $('.menu__content--select.menuable__content__active')
        select2.waitForVisible(10000)
        assert.strictEqual($('.menu__content--select.menuable__content__active .list__tile__title').getText(), 'Normal heart')
      })
      it('should be possible to enter additional notes', () => {
        browser.url('/#/heart-diagnosis/edit')
        browser.waitForExist('.toolbar__title')

        browser.pause(500)

        let notes = $('#textarea-notes')
        notes.clearElement()
        notes.setValue('Notes on the normal heart.')
        $('.save').click()

        browser.pause(500)

        browser.url('/#/heart-diagnosis/')
        browser.waitForVisible('.toolbar__title')

        browser.pause(500)

        let readonlyNotes = $('.content .card .card__text')
        readonlyNotes.waitForVisible(10000)

        assert.strictEqual(readonlyNotes.getText(), 'Notes on the normal heart.')
      })
    })
  })
  // describe('Heart operation', () => {
  //   describe('view', () => {
  //     it('', () => {
  //       browser.url('/#/heart-intervention')
  //       browser.waitForExist('.toolbar__title')
  //     })
  //   })
  //   describe('edit', () => {
  //     it('', () => {
  //       browser.url('/#/heart-intervention/edit')
  //       browser.waitForExist('.toolbar__title')
  //     })
  //   })
  // })
  after(() => {
    logout()
  })
})

describe('Expert workflow', () => {
  // TODO
})

describe('Expert workflow', () => {
  // TODO
})

describe('Albert Amann adds allergy', () => {
  before(() => {
    login('albert@amann.ch', 'AlbertAmann')
  })
  it('allergy should be saved', async function (done) {
    browser.url('/#/medical-info/edit')
    browser.waitForExist('.toolbar__title')

    // set allergy to random value
    var randomValue = Math.random().toString(36).substr(2, 5)
    browser.waitForExist('#textarea-allergy', 10000)
    $('#textarea-allergy').clearElement()
    $('#textarea-allergy').setValue(randomValue)

    logout()
    browser.waitForExist('#login #login-username', 10000)
    login('albert@amann.ch', 'AlbertAmann')

    browser.url('/#/medical-info')
    browser.waitForExist('.toolbar__title')

    // get and compare allergy
    var allergy = $('#allergy').getText()
    assert.strictEqual(allergy, randomValue)

    // just for fun looky looky
  })
  after(() => {
    logout()
  })
})

describe('Albert Amann changes heart center', () => {
  before(() => {
    login('albert@amann.ch', 'AlbertAmann')
  })

  it('removes heart center if present', async function (done) {
    browser.url('/#/edit')
    browser.waitForExist('.toolbar__title')

    var heartCenterSelect = $('#heart-center-select-test-anchor')
    heartCenterSelect.waitForExist(10000)

    // first remove all heartcenters
    browser.waitUntil(function () {
      var selected = heartCenterSelect.$$('.chip')
      if (selected.length === 0) return true
      else {
        selected[0].$('i').click()
        return false
      }
    }, 15000, 'expected zero selected heart center')
  })

  it('adds heart center', () => {
    browser.url('/#/edit')

    browser.pause(500) // elsewise heartCenterSelect cant be found...but why?
    var heartCenterSelect = $('#heart-center-select-test-anchor .input-group__selections')
    heartCenterSelect.waitForExist(10000)
    heartCenterSelect.scroll()
    heartCenterSelect.click()

    var heartCenterSelectMenu = $('.menuable__content__active')
    heartCenterSelectMenu.waitForExist(10000)
    heartCenterSelectMenu.$$('li')[1].click()
    logout()
    login('albert@amann.ch', 'AlbertAmann')

    browser.url('/#/edit')
    browser.waitForExist('.toolbar__title')

    browser.waitUntil(function () {
      return $$('.chip').length >= 1
    }, 10000, 'expected 1 heart center')

    assert.ok($$('.chip').length >= 1)
  })
  after(() => {
    logout()
  })
})

describe('Admin promotes Albert to Expert', () => {
  it('makes sure that Albert is at Insel', async function (done) {
    browser.timeouts('implicit', 10000)
    login('albert@amann.ch', 'AlbertAmann')

    browser.url('/#/edit')
    browser.waitForExist('.toolbar__title')

    var heartCenterSelect = $('#heart-center-select-test-anchor .input-group__selections')
    heartCenterSelect.waitForExist(10000)

    var selected = heartCenterSelect.$$('.chip')
    var foundInsel = false
    selected.forEach(function (element) {
      if (element.getText() === 'insel\ncancel') {
        foundInsel = true
      }
    })
    if (!foundInsel) {
      heartCenterSelect.scroll()
      heartCenterSelect.click()
      var heartCenterSelectMenu = $('.menuable__content__active')
      heartCenterSelectMenu.waitForExist()
      heartCenterSelectMenu.waitForVisible()
      var options = heartCenterSelectMenu.$$('li a div div')
      options.forEach(function (element) {
        if (element.getText() === 'Inselspital Bern') {
          console.log('found insel')
          element.click()
        }
      })
    }

    browser.screenshot()
    browser.saveScreenshot(config.screenshotPath + 'screenshot.png')
    logout()
  })

  // it('upgrades Albert to an expert', async function (done) {
  //   login('admin@insel.ch', 'AdminInsel')
  //   logout()
  // })

  // it('makes sure that Albert has expert interface', async function (done) {
  //   login('albert@amann.ch', 'AlbertAmann')
  //   logout()
  // })
})
