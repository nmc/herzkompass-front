const Filters = {
  install: function (Vue, options) {
    // let re = new RegExp(
    //   '(?=[\d +-.()]{10,})'  // Positive lookahead, at least 10 characters including '[0-9] +-.()'
    //   '((00|\+)\d{1,3}?)[- ]?' +   // GROUP 1: The country code. Optional.
    //   '(\(0\))?'              // Trunk prefix for calls within Switzerland
    //   '[-. (]*' +             // Allow certain non numeric characters that may appear between the Country Code and the Area Code.
    //   '(\d{1,3})' +             // GROUP 2: The Area Code. Required.
    //   '[-. )]*' +             // Allow certain non numeric characters that may appear between the Area Code and the Exchange number.
    //   '(\d{3})' +             // GROUP 3: The Exchange number. Required.
    //   '[-. ]*' +              // Allow certain non numeric characters that may appear between the Exchange number and the Subscriber number.
    //   '(\d{4})' +             // Group 4: The Subscriber Number. Required.
    //   '(?: *x(\d+))?'         // Group 5: The Extension number. Optional.
    // )
    // let re = /((00|\+)\d{1,3}[- ])?[-(]*(\d{2,3})[- )]*(\d{3})[-. ]*((\d{4})|(\d{2})[-. ]*)*(?: *x(\d+))*/gm
    const re2 = /(?=[\d +-.()]{10,})((00|\+)\d{1,3})?[- ]?(\(0\))?[-(]*(\d{1,3})[- )]*(\d{3})[-. ]*((\d{4})|(\d{2})[-. ]*)*(?: *x(\d+))*/gm

    Vue.filter('phonify', (value) => {
      const hasPhonenumber = re2.test(value)
      if (hasPhonenumber) {
        const escaped = value.toString().replace(re2, '<a href="tel:$&">$&</a>')
        console.log(`phonenumber: ${value}, escpaped: ${escaped}`)
        return escaped
      } else {
        return value
      }
    })

    Vue.filter('capitalize', (value) => {
      if (!value) return ''
      value = value.toString()
      return value.toUpperCase()
    })
    Vue.filter('lowercase', (value) => {
      return value.toLowerCase()
    })
  }
}
export default Filters
