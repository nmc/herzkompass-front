import Vue from 'vue'
import store from '../store'
import Router from 'vue-router'
import MedicalInfo from '../components/MedicalInfo.vue'
import Patient from '../components/Patient.vue'
import PatientTable from '../components/elements/PatientTable.vue'
import LoginView from '../components/LoginView.vue'
import HeartCenter from '../components/HeartCenter.vue'
import HeartDiagnosis from '../components/HeartDiagnosis.vue'
import HeartIntervention from '../components/HeartIntervention.vue'
import Success from '../components/signup/Success.vue'
import AccountActivation from '../components/signup/AccountActivation.vue'
import PasswordReset from '../components/signup/PasswordReset.vue'
import PasswordResetConfirm from '../components/signup/PasswordResetConfirm.vue'
import Impressum from '../components/Impressum.vue'
import NotFound from '../components/NotFound.vue'
import AuthenticatedShell from '../components/AuthenticatedShell.vue'
import AnonymousShell from '../components/AnonymousShell.vue'
import { waitOrAbortPendingRequests } from '../utils.js'

Vue.use(Router)

function requireAuth (to, from, next) {
  // check if user is logged in, else redirect to login screen
  if (store.getters.isLoggedIn) {
    // If a routes meta field 'edit' is set to true display edit view
    if (to.matched.some(record => record.meta.edit)) {
      store.commit('CHANGE_MODE', { mode: 'update' })
    } else {
      store.commit('CHANGE_MODE', { mode: 'view' })
    }
    next()
  } else {
    next({
      name: 'login',
      query: { redirect: to.fullPath }
    })
  }
}

function fwdLoginToHome (to, from, next) {
  // If the user is logged in already, forward to profile page
  if (store.getters.isLoggedIn) {
    next({ name: 'home' })
  } else {
    next()
  }
}

const logout = async (to, from, next) => {
  // Make sure, that all requests have responded before logging out.
  try {
    await waitOrAbortPendingRequests()
    store.commit('LOGOUT')
    store.dispatch('resetAppData').then(() => {
      next({ name: 'login' })
    })
  } catch (e) {
    localStorage.clear()
    window.location.reload(true)
  }
}

const router = new Router({
  scrollBehavior: function (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash }
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/',
      components: {
        main: AnonymousShell
      },
      children: [
        {
          path: '',
          components: { main: LoginView },
          name: 'login',
          beforeEnter: fwdLoginToHome
        },
        {
          path: 'signup/password-reset',
          name: 'password-reset',
          components: { main: PasswordReset }
        },
        {
          path: 'signup/password-reset/reset/:uidb64/:token',
          name: 'password-reset-confirm',
          components: { main: PasswordResetConfirm, toolbar: false, sidenav: false, statusbar: false }
        },
        {
          path: 'signup/account-activation/:token',
          name: 'account-activation',
          components: { main: AccountActivation, toolbar: false, sidenav: false, statusbar: false }
        },
        {
          path: 'signup/success',
          name: 'signup-success',
          components: {
            main: Success
          }
        },
        {
          path: 'heart-center',
          name: 'heart-center',
          components: {
            main: HeartCenter
          }
        },
        {
          path: '/impressum-unauthorized',
          name: 'impressum-unauthorized',
          components: {
            main: Impressum
          }
        }
      ]
    },
    {
      path: '/',
      components: {
        main: AuthenticatedShell
      },
      children: [
        {
          path: 'logout',
          name: 'logout',
          beforeEnter: logout
        },
        {
          path: 'profile',
          name: 'home',
          beforeEnter: requireAuth,
          components: {
            main: Patient
          }
        },
        {
          path: 'profile/edit',
          name: 'edit-home',
          beforeEnter: requireAuth,
          components: {
            main: Patient
          },
          meta: { edit: true }
        },
        {
          path: 'medical-info',
          name: 'medical-info',
          beforeEnter: requireAuth,
          components: {
            main: MedicalInfo
          }
        },
        {
          path: 'medical-info/edit',
          name: 'edit-medical-info',
          beforeEnter: requireAuth,
          components: {
            main: MedicalInfo
          },
          meta: { edit: true }
        },
        {
          path: 'patient-list',
          name: 'patient-list',
          beforeEnter: requireAuth,
          components: {
            main: PatientTable
          }
        },
        {
          path: 'patient-list/edit',
          name: 'edit-patient-list',
          beforeEnter: requireAuth,
          components: {
            main: PatientTable
          },
          meta: { edit: true }
        },
        {
          path: 'heart-diagnosis',
          name: 'heart-diagnosis',
          beforeEnter: requireAuth,
          components: {
            main: HeartDiagnosis
          }
        },
        {
          path: 'heart-diagnosis/edit',
          name: 'edit-heart-diagnosis',
          beforeEnter: requireAuth,
          components: {
            main: HeartDiagnosis
          },
          meta: { edit: true }
        },
        {
          path: 'heart-intervention',
          name: 'heart-intervention',
          beforeEnter: requireAuth,
          components: {
            main: HeartIntervention
          }
        },
        {
          path: 'heart-intervention/edit',
          name: 'edit-heart-intervention',
          beforeEnter: requireAuth,
          components: {
            main: HeartIntervention
          },
          meta: { edit: true }
        },
        {
          path: 'impressum',
          name: 'impressum',
          beforeEnter: requireAuth,
          components: {
            main: Impressum
          }
        }
      ]
    },
    {
      components: { toolbar: false, sidenav: false },
      path: '/',
      children: [
        {
          path: '*',
          name: '404',
          component: NotFound
        }
      ]
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  if (Object.keys(from.meta).length > 0 &&
      from.meta.edit &&
      to.name !== 'logout') {
    try {
      await store.dispatch('updateToServer')
      next()
    } catch (err) {
      next(false)
    }
  } else if (Object.keys(from.meta).length > 0 &&
             from.meta.edit &&
             to.name === 'logout') {
    // wait for data being saved before proceeding to logout
    store.dispatch('updateToServer').then(() => { next() })
  } else {
    store.dispatch('updateToServer')
    next(vm => {
      // access to component instance via `vm`
      // console.log(`COMPONENT ${JSON.stringify(vm)}`)
    })
  }
})

export default router
