// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import '../static/css/heartapp-icon-font.css'
import 'vuetify/dist/vuetify.min.css'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import Filters from './filters'
Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(Filters)

const opts = {
  theme: {
    light: true,
    themes: {
      light: {
        primary: '#FF375E',
        accent: '#424242',
        secondary: '#646464',
        info: '#42a5f5',
        warning: '#ffa000',
        error: '#ff5252',
        success: '#81c784',
        indetermined: '#c8c8c8',
        white: '#ffffff',
        black: '#000000',
        unibasGrey: '#282C34'
      }
    }
  },
  icons: {
    iconfont: 'fa' // default - only for display purposes
  }
}

let newWorker
new Vue({ // eslint-disable-line no-new
  el: '#app',
  router,
  store,
  i18n,
  vuetify: new Vuetify(opts),
  created: function () {
    if (navigator.onLine && this.$store.getters.isLoggedIn) {
      this.refreshData()
    }
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/sw.js').then(reg => {
        console.log('registered service worker')
        reg.addEventListener('updatefound', () => {
          console.log('starting sw update')
          newWorker = reg.installing
          newWorker.addEventListener('statechange', () => {
            // Has service worker state changed?
            if (newWorker.state === 'installed' && navigator.serviceWorker.controller) {
              // There is a new service worker available, show the notification
              console.log('new service worker version')
              newWorker.postMessage({ action: 'skipWaiting' })
            }
          })
        })
      }).catch(regError => {
        console.log('SW reg failed: ', regError)
      })
      // let refreshing
      // navigator.serviceWorker.addEventListener('controllerchange', function () {
      //   if (refreshing) return
      //   window.location.reload()
      //   refreshing = true
      // })
    }
  },
  methods: {
    refreshData () {
      this.$store.dispatch('whoami').then(() => {
        this.$store.dispatch('loadAppData')
      })
    }
  },
  render: h => h(App)
})
