import Vue from 'vue'
import createLogger from 'vuex/dist/logger'

import Vuex from 'vuex'

import auth from './modules/auth'
import lang from './modules/lang'
import data from './modules/appData/appData.js'
import drawer from './modules/sideDrawerState.js'
import notification from './modules/notification'
import errorLog from './modules/errorLog.js'
import mode from './modules/view_edit-mode.js'

import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({
  modules: {
    mode,
    auth,
    data,
    drawer,
    lang,
    notification,
    errorLog
  },
  strict: debug,
  plugins: debug ? [createLogger(), createPersistedState()] : [createPersistedState()]
})

export default store

const initialStateCopy = JSON.parse(JSON.stringify(store.state))

export function resetState () {
  store.replaceState(JSON.parse(JSON.stringify(initialStateCopy)))
}
