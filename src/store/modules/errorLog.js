import * as types from '../mutation-types.js'

const namespaced = true

const maxLength = 20 // Maximum length of error log
const state = {
  errors: []
}

const getters = {
  errors: state => {
    return state.errors
  }
}

const actions = {
  logError ({ commit, state }, error) {
    commit(types.APPEND_ERROR, error)
  },
  clearErrorLog ({ commit, state }) {
    commit(types.CLEAR_ERROR_LOG)
  }
}

const mutations = {
  [types.APPEND_ERROR] (state, error) {
    if (state.errors.length < maxLength - 1) {
      state.errors.push(error)
    } else {
      state.errors.shift()
      state.errors.push(error)
    }
  },
  [types.CLEAR_ERROR_LOG] (state) {
    state.errors = []
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
