import Vue from 'vue'
import { httpAction } from '../../common-http'
import generic from './appData/genericApiComponents.js'

const state = {
  tokenString: '',
  isLoggedIn: false,
  userUnderExamination: {}
}

const getters = {
  isLoggedIn: state => {
    return !(state.tokenString.length === 0)
  },
  tokenString: state => {
    return state.tokenString
  },
  userUnderExamination: state => state.userUnderExamination
}

const actions = {
  async login ({ commit, state, dispatch }, credentials) {
    commit('LOGIN')
    try {
      const response = await httpAction('POST', 'rest-auth/login/', credentials)
      commit('LOGIN_SUCCESS', response.key)
      await dispatch('whoami')
      return response
    } catch (e) {
      commit('ABORT_LOGIN')
      throw e
    }
  },
  async signup ({ commit, state, dispatch }, payload) {
    try {
      const response = await httpAction('POST', 'rest-auth/registration/', payload)
      return response
    } catch (e) {
      console.log('signup')
      throw e
    }
  },
  async activateKey ({ commit, state, dispatch }, payload) {
    try {
      const response = await httpAction('POST', 'rest-auth/registration/verify-email/', payload)
      return response
    } catch (e) {
      console.log('activate key')
      throw e
    }
  }
}

const mutations = {
  'ABORT_LOGIN' (state) {
    state.pending = true
  },
  'LOGIN' (state) {
    state.pending = true
  },
  'LOGIN_SUCCESS' (state, token) {
    state.tokenString = `Token ${token}`
    state.pending = false
  },
  'LOGOUT' (state) {
    state.tokenString = ''
    state.pending = false
    state.userUnderExamination = null
  },
  'EXAMINE_USER' (state, newUser) {
    state.userUnderExamination = newUser
  }
}

Vue.util.extend(mutations, generic.mutations)

export default {
  state,
  getters,
  actions,
  mutations
}
