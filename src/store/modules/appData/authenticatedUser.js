import Vue from 'vue'
import { httpAction } from '../../../common-http'
import { handleHTTPError } from '../../../utils'
import generic from './genericApiComponents.js'

const namespaced = true

const initialState = {
  data: null,
  isExpert: false,
  isAdmin: false,
  isOrdinaryUser: true,
  endpoint: 'rest-auth/user'
}
Vue.util.extend(initialState, generic.initialState)
const state = Vue.util.extend({}, initialState)

const getters = {
  isExpert: state => state.isExpert,
  isAdmin: state => state.isAdmin,
  isOrdinaryUser: state => state.isOrdinaryUser
}
Vue.util.extend(getters, generic.getters)

const actions = {
  async getData ({ commit, state, dispatch }) {
    commit('START_UPDATE')
    try {
      const response = await httpAction('GET', `${state.endpoint}/`)
      commit('SET_DATA', response)
      commit('UPDATE_DONE')
      return response
    } catch (error) {
      commit('ADD_NOTIFICATION', {
        actionDesc: 'Who am i',
        message: handleHTTPError(error),
        type: 'error'
      })
      commit('UPDATE_DONE')
    }
  }
}
Vue.util.extend(actions, generic.actions)

const mutations = {
  'SET_DATA' (state, payload) {
    state.data = payload
  },
  'IS_EXPERT' (state, bool) {
    state.isExpert = bool
    state.isOrdinaryUser = !(bool || state.isAdmin)
  },
  'IS_ADMIN' (state, bool) {
    state.isAdmin = bool
    state.isOrdinaryUser = !(bool || state.isExpert)
  },
  'RESET' (state) {
    for (const key in initialState) {
      Vue.set(state, key, initialState[key])
    }
  }
}
Vue.util.extend(mutations, generic.mutations)

export default {
  namespaced,
  state,
  initialState,
  getters,
  actions,
  mutations
}
