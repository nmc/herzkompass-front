import Vue from 'vue'
import generic from './genericApiComponents.js'

const namespaced = true

const initialState = {
  endpoint: 'heart-disease-category'
}
Vue.util.extend(initialState, generic.initialState)
const state = Vue.util.extend({}, initialState)

const getters = Vue.util.extend({}, generic.getters)

const actions = {}
Vue.util.extend(actions, generic.actions)

const mutations = {
  'RESET' (state) {
    for (const key in initialState) {
      Vue.set(state, key, initialState[key])
    }
  }
}
Vue.util.extend(mutations, generic.mutations)

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
