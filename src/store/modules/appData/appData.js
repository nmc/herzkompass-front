import { handleHTTPError } from '../../../utils'
import authenticatedUser from './authenticatedUser'
import user from './user'
import relativeInfo from './relativeInfo'
import medicalInfo from './medicalInfo'
import heartDiagnosis from './heartDiagnosis'
import heartCenter from './heartCenter'
import heartIntervention from './heartIntervention'

import heartDiseaseCategories from './heartDiseaseCategories.js'
import heartOperationCategories from './heartOperationCategories.js'
import heartDisease from './heartDisease.js'
import heartOperation from './heartOperation.js'

const actions = {
  whoami ({ commit, state, dispatch }) {
    return dispatch('authenticatedUser/getData').then(() => {
      commit('authenticatedUser/IS_ADMIN',
        state.authenticatedUser.data.groups.indexOf('admin') > -1)
      commit('authenticatedUser/IS_EXPERT',
        state.authenticatedUser.data.groups.indexOf('expert') > -1)
    }).catch((error) => {
      commit('ADD_NOTIFICATION', {
        actionDesc: 'Who am i',
        message: handleHTTPError(error),
        type: 'error'
      })
    })
  },

  loadAppData ({ commit, state, dispatch }, user) {
    dispatch('user/getSchema')
    dispatch('user/getList').then((list) => {
      commit('EXAMINE_USER', list.find(user =>
        user.pk === state.authenticatedUser.data.pk
      ))
    })
    for (const endpoint in modules) {
      if (endpoint !== 'user') { // user has already been loaded above
        dispatch(`${endpoint}/getList`)
        dispatch(`${endpoint}/getSchema`)
      }
    }
  },

  async updateToServer ({ commit, state, dispatch }) {
    const updates = []
    const writableEndpointList = [
      'user',
      'relativeInfo',
      'medicalInfo',
      'heartDiagnosis',
      'heartIntervention'
    ]
    for (const endpoint of writableEndpointList) {
      updates.push(dispatch(`${endpoint}/deleteObjectsFromList`))
      await Promise.all(updates)
      updates.push(dispatch(`${endpoint}/postObjectsFromList`))
      updates.push(dispatch(`${endpoint}/patchObjectsFromList`))
    }
    const response = await dispatch('medicalInfo/patchObjectsFromList')
    return response
  },

  resetAppData ({ commit, state, dispatch }) {
    for (const moduleName in modules) {
      commit(`${moduleName}/RESET`)
    }
  }
}

const getters = {
  isLoading: state => {
    return Object.keys(modules).reduce(
      (accu, module) => accu || state[module].isLoading
      , false
    )
  }
}

const modules = {
  user,
  authenticatedUser,
  medicalInfo,
  heartCenter,
  relativeInfo,
  heartDiagnosis,
  heartDisease,
  heartDiseaseCategories,
  heartIntervention,
  heartOperation,
  heartOperationCategories
}

export default {
  actions,
  getters,
  modules
}
