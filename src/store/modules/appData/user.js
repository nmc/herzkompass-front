import Vue from 'vue'
import generic from './genericApiComponents.js'
import { httpAction } from '../../../common-http'

const namespaced = true

const initialState = {
  endpoint: 'users',
  syncJobs: {
    createList: [], // list of new objects that will be posted to the server
    updateList: [], // list of id/fielna
    deleteList: []
  }
}
Vue.util.extend(initialState, generic.initialState)
const state = Vue.util.extend({}, initialState)

const getters = Vue.util.extend({
  syncJobs: state => state.syncJobs
}, generic.getters)

const actions = {
  async deleteProfile ({ commit, state, dispatch }, id) {
    let response
    try {
      commit('START_UPDATE')
      response = await httpAction('DELETE', `users/${id}`)
      return response
    } finally {
      commit('UPDATE_DONE')
    }
  }
}
Vue.util.extend(actions, generic.actions)

const mutations = {
  'TOGGLE_EXPERT' (state, id) {
    const user = state.list.find(usr => usr.pk === id)
    const index = user.groups.indexOf('expert')
    if (index >= 0) {
      // js magic! lodash' remove function is not detected by vuex computed
      // _.remove(user.groups, (group) => { return group === 'expert' })
      user.groups.splice(index, 1)
    } else {
      user.groups.push('expert')
    }
  },
  'RESET' (state) {
    for (const key in initialState) {
      Vue.set(state, key, initialState[key])
    }
  }
}
Vue.util.extend(mutations, generic.mutations)

export default {
  state,
  namespaced,
  getters,
  actions,
  mutations
}
