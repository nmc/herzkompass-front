import Vue from 'vue'
import generic from './genericApiComponents.js'

const namespaced = true

const initialState = {
  endpoint: 'relative',
  syncJobs: {
    createList: [], // list of new objects that will be posted to the server
    updateList: [], // list of id/fielna
    deleteList: []
  }
}
Vue.util.extend(initialState, generic.initialState)
const state = Vue.util.extend({}, initialState)

const getters = Vue.util.extend({
  syncJobs: state => state.syncJobs
}, generic.getters)

const actions = {}
Vue.util.extend(actions, generic.actions)

const mutations = {
  'RESET' (state) {
    for (const key in initialState) {
      Vue.set(state, key, initialState[key])
    }
  }
}
Vue.util.extend(mutations, generic.mutations)

export default {
  state,
  namespaced,
  getters,
  actions,
  mutations
}
