import { httpAction } from '../../../common-http'
import { handleHTTPError } from '../../../utils'
import Vue from 'vue'

const initialState = {
  schema: null, // OPTIONS on /endpoint/
  list: [], // GET on /endpoint/
  isLoading: true,
  updateCounter: 0,
  fieldErrorMessages: []
}

const getters = {
  data: state => state.data,
  list: state => state.list,
  schema: state => state.schema,
  isLoading: state => state.isLoading,
  fieldErrors: state => state.fieldErrorMessages
}

const actions = {
  async getList ({ commit, state, dispatch }) {
    commit('START_UPDATE')
    try {
      const response = await httpAction('GET', `${state.endpoint}/`)
      commit('SET_LIST', response)
      commit('UPDATE_DONE')
      return response
    } catch (error) {
      commit('ADD_NOTIFICATION', {
        actionDesc: 'Get list',
        message: handleHTTPError(error),
        type: 'error'
      })
      commit('UPDATE_DONE')
    }
  },

  async getSchema ({ commit, state, dispatch }) {
    // get schema of resource first
    // return promise so the parent function can catch up when it is resolved
    commit('START_UPDATE')
    try {
      const response = await httpAction('OPTIONS', `${state.endpoint}/`)
      commit('SET_SCHEMA', response)
      commit('UPDATE_DONE')
    } catch (error) {
      commit('ADD_NOTIFICATION', {
        actionDesc: 'Get Schema',
        message: handleHTTPError(error),
        type: 'error'
      })
      commit('UPDATE_DONE')
    }
  },

  postObjectsFromList ({ commit, state, dispatch }) {
    return state.syncJobs.createList.forEach(async item => {
      commit('START_UPDATE')
      try {
        const response = await httpAction('POST', `${state.endpoint}/`, item)
        commit('UPDATE_DONE', { operation: 'create', obj: item })
        // doing an getList dispatch means another server request.  We
        // already receviced the new data in the response (all we are
        // interested in is the id that was set by the server). So lets
        // uncomment getList and set the id from the response:
        // dispatch('getList')
        item.id = response.id
        return response
      } catch (error) {
        commit('ADD_NOTIFICATION', {
          actionDesc: 'Post objects',
          message: handleHTTPError(error),
          type: 'error'
        })
        commit('UPDATE_DONE', { operation: 'create', obj: item })
      }
    })
  },

  deleteObjectsFromList ({ commit, state, dispatch }) {
    return state.syncJobs.deleteList.forEach(async item => {
      commit('START_UPDATE')
      try {
        const response = await httpAction('DELETE', `${state.endpoint}/${item.id}/`)
        commit('UPDATE_DONE', { operation: 'delete', obj: item })
        return response
      } catch (error) {
        commit('ADD_NOTIFICATION', {
          actionDesc: 'Delete objects from list',
          message: handleHTTPError(error),
          type: 'error'
        })
        commit('UPDATE_DONE', { operation: 'delete', obj: item })
        return error
      }
    })
  },

  async patchObjectsFromList ({ commit, state, dispatch }) {
    const responseL = []
    for (const item of state.syncJobs.updateList) {
      commit('START_UPDATE')
      try {
        const response = await httpAction('PATCH', `${state.endpoint}/${item.id}/`, item)
        commit('UPDATE_DONE', { operation: 'update', obj: item })
        dispatch('getList')
        responseL.push(response)
      } catch (e) {
        commit('ADD_NOTIFICATION', {
          actionDesc: this.$t('error.patch'),
          message: e,
          type: 'error'
        })
        // commit('ADD_FIELD_ERROR', e)
        commit('UPDATE_DONE', { operation: 'update', obj: item })
        throw e
      }
    }
  }
}

const mutations = {
  'START_UPDATE' (state) {
    state.updateCounter++
    state.isLoading = true
  },

  'UPDATE_DONE' (state, payload) {
    if (state.updateCounter > 0) state.updateCounter--
    state.isLoading = !(state.updateCounter === 0)

    if (payload && payload.obj) {
      state.syncJobs[`${payload.operation}List`].splice(
        state.syncJobs[`${payload.operation}List`].indexOf(payload.obj), 1
      )
    }
  },

  'SET_LIST' (state, payload) {
    state.list = payload
  },

  'SET_SCHEMA' (state, payload) {
    state.schema = payload.actions.POST
  },

  'DEL_ITEM' (state, obj) {
    if (obj.id) {
      state.syncJobs.deleteList.push(obj)
      const toBeUpdated = state.syncJobs.updateList.filter(x => x.obj.id === obj.id)
      toBeUpdated.forEach(x => {
        state.syncJobs.updateList.splice(
          state.syncJobs.updateList.indexOf(x), 1
        )
      })
    } else {
      state.syncJobs.createList.splice(state.syncJobs.createList.indexOf(obj), 1)
    }
    state.list.splice(
      state.list.indexOf(obj), 1
    )
  },
  'ADD_FIELD_ERROR' (state, data) {
    Vue.set(state, 'fieldErrorMessages', data.data)
  },

  'ADD_OBJECT' (state, obj) {
    state.list.push(obj)
  },

  'UPDATE_ATTRIBUTE' (state, payload) {
    Vue.set(payload.fieldObject, payload.fieldName, payload.data)
    // add object either to createObject (if it is a new one) or updateObject
    if (payload.fieldObject.id) { // object exists on server
      const uList = state.syncJobs.updateList
      if (!uList.find(entry => entry === payload.fieldObject)) {
        // an entry for this object does not exist in our updateList
        uList.push(payload.fieldObject)
      }
    } else { // new object, add it to createJobs if not there yet
      const cList = state.syncJobs.createList
      if (!cList.find(obj => obj === payload.fieldObject)) {
        cList.push(payload.fieldObject)
      }
    }
  }
}

export default {
  initialState,
  getters,
  actions,
  mutations
}
