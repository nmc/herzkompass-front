const state = {
  mode: 'view'
}

const MODES = ['read', 'update']

const getters = {
  mode: state => {
    return state.mode
  }
}

const mutations = {
  'CHANGE_MODE' (state, mode) {
    state.mode = mode
  }
}

export default {
  MODES,
  state,
  getters,
  mutations
}
