const state = {
  showDrawer: true,
  showMini: false
}

const getters = {
  showDrawer: state => {
    return state.showDrawer
  },
  showMini: state => {
    return state.showMini
  }
}

const mutations = {
  'SET_DRAWER' (state, value) {
    state.showDrawer = value
  },
  'HIDE_DRAWER' (state) {
    state.showDrawer = false
  },
  'TOGGLE_DRAWER' (state) {
    state.showDrawer = !state.showDrawer
  },
  'CIRCLE_DRAWER_STATE' (state) {
    if (!state.showDrawer) {
      state.showMini = true
      state.showDrawer = true
    } else if (state.showMini) {
      state.showMini = false
    } else if (!state.showMini) {
      state.showDrawer = false
    }
  }
}

export default {
  state,
  getters,
  mutations
}
