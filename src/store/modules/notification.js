import Vue from 'vue'

const maxLength = 5

const state = {
  notificationList: [],
  notificationNewsList: [],
  showBottomSheet: false
}

class Notification {
  constructor (actionDesc, type, message) {
    if (!['success', 'info', 'error', 'debug'].includes(type)) {
      throw new Error('Notification type must be either \'success\', \'debug\' , \'info\' or \'error\'.')
    }
    if (message.length < 3) {
      throw new Error('Notification message must not be empty.')
    }
    this.date = Date.parse(new Date()) // time now in seconds since day zero
    this.type = type
    this.actionDesc = actionDesc // manual description of the action that failed
    this.message = message
  }
}

const getters = {
  notificationList: state => { // persitent list for logging
    return state.notificationList
  },
  notificationNewsList: state => { // temporary list for eg. snackbar
    return state.notificationNewsList
  },
  showBottomSheet: state => {
    return state.showBottomSheet
  }
}

const mutations = {
  'ADD_NOTIFICATION' (state, notification) {
    const noti = new Notification(notification.actionDesc, notification.type, notification.message)
    state.notificationNewsList.unshift( // unshift adds an elem to the beginning of an array.
      noti
    )
    state.notificationList.unshift( // unshift adds an elem to the beginning of an array.
      noti
    )
    if (state.notificationNewsList.length > maxLength) {
      state.notificationNewsList.pop()
    }
  },
  'REMOVE_PERSISTENT_NOTIFICATION' (state, index) {
    if (index === 'undefined' || index === null || index < 0) state.notificationList.pop() // removes last element
    else state.notificationList.splice(index, 1)
  },
  'REMOVE_NOTIFICATION' (state, index) {
    if (index === 'undefined' || index === null || index < 0) state.notificationNewsList.pop() // removes last element
    else state.notificationNewsList.splice(index, 1)
  },
  'TOGGLE_BOTTOMSHEET' (state, bool) {
    if (typeof bool === 'undefined') {
      state.showBottomSheet = !state.showBottomSheet
      Vue.set(state, 'notificationNewsList', [])
    } else {
      state.showBottomSheet = bool
    }
  }
}

export default {
  state,
  getters,
  mutations
}
