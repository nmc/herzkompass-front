let lang = 'en'
if (window.navigator.language.substring(0, 2) === 'fr') {
  lang = 'fr'
} else if (window.navigator.language.substring(0, 2) === 'de') {
  lang = 'de'
} else { lang = 'en' }

const state = {
  uiLanguage: lang
}

const getters = {
  uiLanguage: state => {
    return state.uiLanguage
  }
}

const mutations = {
  'CHANGE_LANGUAGE' (state, language) {
    state.uiLanguage = language
  }
}

export default {
  state,
  getters,
  mutations
}
