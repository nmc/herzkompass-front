/* eslint no-extend-native: ["error", { "exceptions": ["Error"] }] */
import i18n from './i18n.js'
import store from './store'

const delay = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export const waitOrAbortPendingRequests = async (timeout = 5000) => {
  const laps = 5
  for (let i = 0; i < 5; i++) {
    if (store.getters.isLoading) {
      await delay(timeout / laps)
      continue
    } else {
      return 'Synchronisation abgeschlossen.'
    }
  }
  throw new Error('Synchronisierung mit dem dauerte mehr als 5 Sekunden und wurde abgebrochen.')
}

export const formatDate = date => {
  const d = new Date(date) // create date object from number
  const t = new Date()
  let options
  if (t.getFullYear() === d.getFullYear()) {
    if (t.getMonth() === d.getMonth() && t.getDay() === d.getDay()) {
      options = { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false }
    } else {
      options = { weekday: 'short', month: 'short', day: 'numeric' }
    }
  } else {
    options = { year: 'numeric', month: 'short', day: 'numeric' }
  }
  const language = store.getters.uiLanguage
  const formattedDate = new Intl.DateTimeFormat(language, options).format(d)
  return formattedDate
}

export function handleHTTPError (error, additionalInfos = '') {
  let errorString = ''
  console.log('ERROR: ')
  console.log(error)

  errorString = i18n.t('app.error')
  errorString += ': \n' + additionalInfos

  if (!('toJSON' in Error.prototype)) {
    Object.defineProperty(Error.prototype, 'toJSON', {
      value: function () {
        const alt = {}
        Object.getOwnPropertyNames(this).forEach(function (key) {
          alt[key] = this[key]
        }, this)
        return alt
      },
      configurable: true,
      writable: true
    })
  }

  store.dispatch('errorLog/logError', {
    date: Date.now(),
    user: store.getters['selectedUser/data'],
    error: JSON.stringify(error),
    additionalInfos
  })
  return errorString
}
