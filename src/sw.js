// navigator.storageQuota.queryInfo('temporary').then(function (info) {
//   console.log(` Quota: ${info.quota}`)
//   // Result: <quota in bytes>
//   console.log(` Usage: ${info.usage}`)
//   // Result: <used data in bytes>
// })
var CACHE_VERSION = 'heartapp-v3'
var urlsToCache = [
  '/',
  'heartapp-icon-font.woff',
  'static/img/logo.png',
]

self.addEventListener('install', function (event) {
  // var now = Date.now()
  // console.log(`Installed SW on ${now}`)
  event.waitUntil(
    caches.open(CACHE_VERSION).then(
      function (caches) {
        // console.log('Opened cache')
        return caches.addAll(urlsToCache)
      }
    )
    // .finally(function () { return self.clients.claim() })
  )
})

self.addEventListener('activate', function (event) {
  var cacheKeeplist = [`${CACHE_VERSION}`]
  console.log(`Activating sw with cache version: ${CACHE_VERSION}`)
  // delete all entries in cache except current cache version
  event.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        console.log('checking cache key', key)
        if (cacheKeeplist.indexOf(key) === -1) {
          console.log('deleting cache key', key)
          return caches.delete(key)
        }
      }))
    })
  )
  // waitUntill can be call several times
  event.waitUntil(self.clients.claim())
})

self.addEventListener('fetch', function (event) {
  console.log('request headers', event.request.url)
  event.respondWith(
    caches.match(event.request).then(function (cachedResponse) {
      if (cachedResponse) {
        // console.log(`found response for\n ${event.request.url}`)
        return cachedResponse
      }
      return fetch(event.request).then(function (response) {
        // if valid response and image, we want to add it to our cache
        if (response && response.status === 200 &&
          event.request.headers.get('Accept') === 'image/png') {
          var responseToCache = response.clone()
          caches.open(CACHE_VERSION).then(function (cache) {
            // console.log(`saving response to\n${event.request.url}:\n${responseToCache}`)
            cache.put(event.request, responseToCache).catch(
              (error) => console.log(`cache put error:\n${error}`)
            )
          }).catch((error) => console.log(`cache open error:\n${error}`))
        }
        return response
      }).then(function (data) {
        // console.log(`fetched data:\n${JSON.stringify(data)}`)
        return data
      })
    })
  )
})

self.addEventListener('message', function (event) {
  // console.log('got message')
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting()
  }
})
