import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export const LANGUAGES = [
  {
    name: 'English',
    code: 'en'
  },
  {
    name: 'Deutsch',
    code: 'de'
  },
  {
    name: 'French',
    code: 'fr'
  }
]

const messages = {}

for (const key in LANGUAGES) {
  if (Object.prototype.hasOwnProperty.call(LANGUAGES, key)) {
    const code = LANGUAGES[key].code
    messages[LANGUAGES[key].code] = require(`./locales/${code}.yml`)
  }
}

let uiLanguage

try {
  uiLanguage = JSON.parse(window.localStorage.vuex).lang.uiLanguage
} catch (e) {}

const language = uiLanguage || window.navigator.userLanguage || window.navigator.language || 'de'
Vue.config.lang = language.substring(0, 2)

const i18n = new VueI18n({
  locale: Vue.config.lang,
  fallbackLocale: 'en',
  silentTranslationWarn: false,
  messages
})

Vue.prototype.$locale = {
  change (lang) {
    i18n.locale = lang
  },
  current () {
    return i18n.locale
  }
}

export default i18n
