import store from './store'

let apihost
switch (process.env.NODE_ENV) {
  case 'production':
    apihost = 'https://api.heartapp.org'
    break
  default:
    apihost = 'http://localhost:8000'
    break
}

export const apiHost = apihost

export const httpAction = async (method = 'GET', endpoint = '', data = {}) => {
  store.commit('START_UPDATE')
  const request = { // generic request data object
    method, // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer',
    headers: {
      'Accept-Language': store.getters.uiLanguage,
      Authorization: store.getters.tokenString,
      'Content-Type': 'text/html; charset=utf-8'
    }
  }
  if (Object.keys(data).length > 0 && data.constructor === Object) {
    // if there is js body data, this means that we want to do an json request
    // and attach the stringified js object to the body:
    request.body = JSON.stringify(data)
    request.headers['Content-Type'] = 'application/json'
  }
  const response = await fetch(`${apihost}/${endpoint}`, request)
  try {
    if (!response.ok) {
      const formFieldReport = await response.json()
      throw formFieldReport
    }
    if (response.status === 204) { // no content
      return 0
    }
    const ret = await response.json()
    return ret
  } catch (e) {
    const collectErrorObject = {
      httpError: response.statusText,
      data: e // formFieldReport from above oder response.json() error
    }
    throw collectErrorObject
  } finally {
    store.commit('UPDATE_DONE')
  }
}

export const ENDPOINTS = {
  PASSWORD_RESET: 'rest-auth/password/reset/',
  PASSWORD_RESET_CONFIRM: 'rest-auth/password/reset/confirm/',
  PASSWORD_CHANGE: 'rest-auth/password/change/'
}
