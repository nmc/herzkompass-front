---
title: Benutzerdokumentation 'heartapp.org'
titlegraphic: ./doc/img/annotated/logo.png
author: New Media Center - Universität Basel
date: \today
papersize: a4paper
toc: yes
lang: de
documentclass: report
subparagraph: yes
---

# Einleitung

Vielen Dank für Ihr Interesse an der Web Applikation [HeartApp][d433c6de]!

Diese Applikation entstand in einer Zusammenarbeit zwischen dem New Media
Center der Universität Basel und dem Inselspital Bern. HeartApp stellt
Patienten mit einem angeborenen Herzfehler wichtige medizinische Informationen
auf dem Smartphone zur Verfügung.

# Rollen

Die Applikation [HeartApp][d433c6de] besitzt drei verschiedene Benutzerrollen:
der Patient, der Experte und der Administrator. Jedem Benutzer von
[HeartApp][d433c6de] ist mindestens eine dieser Rollen zugewiesen. Mehrfache
Rollenzuweisungen sind möglich sind. Im Folgenden werden diese verschiedenen
Rollen genauer erläutert.

## Patient

Der Patient ist der Standardbenutzer der App. Jede Person, die sich im System
registriert, wird zuerst als Patient erfasst. Jeder Patient kann unabhängig von
der Zugehörigkeit zu einem Herzzentrum Informationen über seinen Herzfehler
sammeln und erfassen.

## Experte

Die Experten haben die Möglichkeit, auf alle Daten der Patienten Ihres
Herzzentrums zuzugreifen und diese zu verifizieren. Administratoren eines
Herzzentrums können die Expertenrolle an an das Herzzentrum geknüpfte Benutzer
zuweisen.

## Administrator
Administratoren werden durch einen Super-Administrator festgelegt (aktuell das
[NMC][326f629c]). Jedes Herzzentrum verfügt über mindestens einen
Administrator. Dieser ist verantwortlich für die Ernennung von Experten. Ein
Administrator kann gleichzeitig auch Experte sein.

## Herzzentrum
Ein Patient hat die Möglichkeit, sich einem oder mehreren Herzzentren
anzuschliessen. Experten dieser Zentren können alle Daten der angeschlossenen
Patienten uneingeschränkt editieren.

Neue Herzzentren können nur durch den Super-Administrator (aktuell das
[NMC][326f629c]) hinzugefügt werden.

# Systemanforderungen
Damit Sie die Applikation einwandfrei nutzen können, benötigen Sie einen
Web Browser. Wir empfehlen einen aktuellen [Firefox][ab6dbf32] oder
[Chrome][1eaa4eb6] Browser. Der Internet Explorer wird ab Version 11.0
unterstützt. Der in neueren Windows Versionen enthaltene Edge Browser
funktioniert ebenfalls.

  [d433c6de]: https://www.heartapp.org "HeartApp"
  [326f629c]: https://nmc.unibas.ch "New Media Center"
  [ab6dbf32]: https://www.mozilla.org/firefox/ "Firefox"
  [1eaa4eb6]: https://www.google.com/chrome/ "Chrome"
