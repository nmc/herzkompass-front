# Administrator
Für Administratoren ist es sinnvoll, die Applikation an einem Desktop Computer zu bedienen.
Öffnen Sie die [HeartApp][83a6c1bc] in ihrem Browser.

Als Administrator können Sie einzelnen Benutzern, die mit Ihrem Herzzentrum
verbunden sind, die Möglichkeit geben, Patientendaten zu bearbeiten und zu
verifizieren.

  [83a6c1bc]: https://www.heartapp.org "Heartapp"

## Login
Loggen Sie sich mit einem Benutzer, der über Administratorenrechte verfügt, in
das System ein.

![Login Screen](./doc/img/annotated/UI_start.png)\

Sie gelangen nun auf die Profilseite des Administrators:

![Benutzerprofil des Administrators](./doc/img/annotated/Admin_Benutzerprofil_anschauen.png)\

Um einem Benutzer die Möglichkeit zu geben Patienten Ihres Herzzentrums zu
bearbeiten und zu verifizieren, müssen Sie diesen zum Experten ernennen.
Klicken Sie dafür auf [`Experte ernennen`][c11248c3].

  [c11248c3]: https://www.heartapp.org/#/expert-list "Experte ernennen"

## Experten ernennen

Um einem Benutzer den Experten-Status zu geben, klicken Sie im seitlichen `Menü` erst auf den Schieberegler `Daten bearbeiten` und dann auf das `Auswahlfeld` in der Spalte `Experte`:

![Experte ernennen](./doc/img/annotated/Admin_Experte_ernennen.png)\

Falls sich der gewünschte Benutzer nicht auf der ersten Seite befindet,
blättern Sie mit den `Bedienelementen` unterhalb der Liste zum gewünschten Benutzer.

Damit Ihre Auswahl gespeichert wird, klicken Sie erneut auf den Schieberegler `Daten bearbeiten` oder eine beliebige andere Seite in der
Administrationsoberfläche oder melden Sie sich vom System durch einen Klick auf
[`Logout`][b5cc11d9] ab.

  [b5cc11d9]: https://www.heartapp.org/#/logout "Logout"
