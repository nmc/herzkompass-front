# Patient
Wir stellen die [HeartApp][83a6c1bc] als [Progressive Web App][529c9fe8] zur
Verfügung. Sie können die Applikation direkt von der Webseite installieren. Sie
ist nicht im Appstore erhätlich.

PWAs werden durch aktuelle Versionen der mobilen Chrome & Firefox Browser
unterstützt. Patienten mit einem iPhone können PWAs ab der Version 11.1 in der
iOS Version 11.3 verwenden.

  [83a6c1bc]: https://www.heartapp.org "Heartapp"
  [529c9fe8]: https://de.wikipedia.org/wiki/Progressive_Web_App "Progressive Web App"

## Registrieren

Falls Sie noch keinen Zugang zum System haben, legen Sie sich einen neuen
Benutzer an. Klicken Sie dafür auf die Registerkarte `REGISTRIEREN`, um die
Ansicht für die Registrierung zu öffnen:

![Benutzer registrieren](./doc/img/annotated/UI_Benutzer_registrieren.png)\

Füllen Sie das Formular vollständig aus und bestätigen Sie durch
einen Klick auf `ANMELDEN`. Sie erhalten nun vom System eine Email um Ihre
Adresse zu bestätigen. Klicken Sie auf den darin enthaltenen Link um Ihren
Benutzer zu aktivieren.


## Einloggen

Melden Sie sich mit Ihrem Benutzernamen im System an.

![Login Screen](./doc/img/annotated/UI_start.png)\

Sie gelangen auf die Profilseite Ihres Benutzers:

![Benutzerprofil des Patienten](./doc/img/annotated/Patient_Benutzerprofil_anschauen.png)\

### Passwort vergessen?

Wenn Sie ihr Passwort vergessen haben, tippen Sie auf der Startseite auf den
Link [`PASSWORT VERGESSEN?`][97a71b56]. Geben Sie Ihre Email-Adresse ein und
tippen Sie auf den Link `PASSWORT ZURÜCKSETZEN`. Sie erhalten nun per Mail eine
Nachricht in der das weitere Vorgehen beschrieben ist.

  [97a71b56]: https://www.heartapp.org/#/signup/password-reset "Passwort vergessen"

## Benutzerdaten bearbeiten
Haben Sie sich erfolgreich angemeldet, gelangen Sie auf Ihr
[Benutzerprofil][f815d815]. Möchten Sie Ihre Benutzerdaten ändern, können Sie
dies hier tun. Tippen Sie dafür im Menü auf den Schieberegler [`Daten
bearbeiten`][4d9b3642].

Damit Ihre Daten durch medizinisches Fachpersonal geprüft werden können, müssen
Sie Ihr behandelndes Herzzentrum auswählen.

Weiter haben Sie die Möglichkeit, Angehörige zu erfassen, die im Bedarfsfall
benachrichtig werden sollen.

  [f815d815]: https://www.heartapp.org/#/ "Benutzerprofil"
  [4d9b3642]: https://www.heartapp.org/#/edit "Daten bearbeiten"

## Medizinische Informationen erfassen
Um Ihre medizinischen Daten zu erfassen wählen Sie aus dem Menü den Punkt
[`Medizinische Info`][5cdbd976]. Hier haben Sie die , medizinisch relevanten
Daten einzusehen oder zu bearbeiten.

Sie können Informationen in folgenden Kategorien einsehen und via dem
Schieberegler [`Daten bearbeiten`][53b0c42f] editieren.

- Ihrem Kardiologen
- Ihrem Hausarzt
- Ihrem Pflegekontakt
- Den Komplikationen
- Allergien
- Nichtkardiale Diagnosen
- Medikamente die Sie einnehmen müssen
- Oraler Antikoagulation
- Einschränkungen beim Sport
- Informationen zur Verhütung
- Endocarditis Prophylaxe
- ICD
- Herzschrittmacher
- Luftfilter
- Patientenverfügung
- Organspenderausweis


  [5cdbd976]: https://www.heartapp.org/#/medical-info "Medizinische Info"
  [53b0c42f]: https://www.heartapp.org/#/medical-info/edit "Medizinische Infos bearbeiten"

## Herzfehler erfassen
Um Ihren Herzfehler einzusehen und zu bearbeiten, wählen Sie im Menü den Punkt
[`Herzfehler`][04fcf327].
Hier finden Sie die genaue Bezeichnung und eine schematische Darstellung Ihres
Herzfehlers sowie Notizen zu Eigenheiten Ihrer Diagnose.

  [04fcf327]: https://www.heartapp.org/#/heart-diagnosis "Herzfehler"


## Herzoperation erfassen

Um Ihre Herzoperation einzusehen und zu bearbeiten wählen Sie im Menü den Punkt
[`Herzoperation`][04fcf3ee].
Hier finden Sie die genaue Bezeichnung und eine schematische Darstellung Ihrer
Herzoperation sowie Notizen zu Eigenheiten Ihrer Operationen.

  [04fcf3ee]: https://www.heartapp.org/#/heart-intervention "Herzoperation"

## Daten verifizieren

In der Statusleiste am oberen Bildschirmrand sehen Sie, ob Ihre Daten durch
medizinisches Fachpersonal geprüft wurden. Hat ein Experte Ihre Daten geprüft,
erscheint ein grüner Haken `Geprüft`. Wurden Ihre Daten noch nicht überprüft,
steht an Stelle des grünen Hakens ein rotes Ausrufezeichen und `Ungeprüft`.
Möchten Sie sicher gehen, dass Ihre Daten korrekt sind, setzen Sie sich mit
Ihrem Herzzentrum in Verbindung, damit jemand mit dem nötigen Fachwissen Ihre
Daten prüfen kann.

Wichtig: Sind Ihre Daten bereits verifiziert und nehmen Sie Änderungen daran
vor, müssen die Daten erneut geprüft werden.

## Sprache wechseln
Um die Sprache zu wechseln, tippen Sie auf das Weltkugelsymbol in der roten
Leiste am oberen Rand und wählen Sie die gewünschte Sprache aus.

![Sprache wechseln](./doc/img/annotated/UI_Sprache_wechseln.png)\

Die Benutzeroberfläche sowie die Bezeichnung der Herzfehler und Herzoperation
erscheinen nun in der von Ihnen gewählten Sprache.

