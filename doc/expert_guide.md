# Experte
Für Experten ist es sinnvoll, die Applikation an einem Desktop Computer zu bedienen.
Öffnen Sie die [HeartApp][83a6c1bc] in ihrem Browser.

  [83a6c1bc]: https://www.heartapp.org "Heartapp"

Als Experte haben Sie die Möglichkeit, die Daten von einzelnen Patienten, die
mit Ihrem Herzzentrum verbunden sind, zu bearbeiten und zu verifizieren.

## Registrieren
Falls Sie noch keinen Zugang zum System haben, legen Sie sich am Besten mit der
Email-Adresse Ihres Herzzentrums einen neuen Benutzer an.

![Benutzer registrieren](./doc/img/annotated/UI_Benutzer_registrieren.png)\

Klicken Sie dafür auf die Registerkarte `REGISTRIEREN`, füllen Sie das
nachfolgende Formular vollständig aus und bestätigen Sie durch einen Klick auf
`ANMELDEN`. Sie erhalten nun vom System eine Email, um Ihre Adresse zu
bestätigen. Klicken Sie auf den darin enthaltenen Link, um Ihr Konto zu
aktivieren.


### Login

Melden Sie sich mit Ihrem Benutzer im System an.

![Login Screen](./doc/img/annotated/UI_start.png)\

Sie gelangen auf die Profilseite Ihres Benutzers:

![Benutzerprofil des Patienten](./doc/img/annotated/Patient_Benutzerprofil_anschauen.png)\

### Mit Herzzentren verbinden

Damit Sie Zugriff auf die Daten der Patienten Ihres Herzzentrums erlangen
können, müssen Sie erst noch festlegen, welchem Herzzentrum / welchen
Herzzentren Sie angehören.

Legen Sie dafür den Schalter [`Daten bearbeiten`][7220f0f7] im Seitenmenü Ihres
Benutzerprofils um. Unter dem Punkt `Bitte wählen Sie Ihr Herzzentrum` haben
Sie nun die Möglichkeit, Herzzentren, die sich an der App beteiligen,
auszuwählen.

  [7220f0f7]: https://www.heartapp.org/#/edit "Daten bearbeiten"

Melden Sie sich anschliessend bei einem Administrator Ihres Herzzentrums, der
Ihnen die benötigten Berechtigungen ausstellen kann.

## Benutzerdaten bearbeiten / verifizieren
Nachdem Sie sich erfolgreich im System angemeldet haben, gelangen Sie auf die
Ausgangsseite Ihres Benutzers. Klicken Sie nun auf [`Patient
auswählen`][f7a25e45], um eine Liste mit Patienten zu laden.

![Benutzerprofil des Experten](./doc/img/annotated/Experte_Benutzerprofil_anschauen.png)\

  [f7a25e45]: https://www.heartapp.org/#/patient-list "Patient auswählen"

Wählen Sie nun einen Patienten Ihres Herzzentrums aus der `Liste` mit einem klick auf das `Auge` (um die Daten eines Patienten anzusehen) oder `Stift` (um die Daten zu bearbeiten) Symbol aus. Falls
sich der gewünschte Patient nicht auf der ersten Seite befindet, benutzen Sie
`die Bedienelemente` unterhalb der Liste, um zu Blättern, oder sortieren Sie die Liste durch Klicken auf die Spaltentitel neu.

![Liste der Patienten](./doc/img/annotated/Experte_Patient_auswaehlen.png)\

Haben Sie einen Patienten, in dieser Abbildung `Albert Amann`, ausgewählt, werden dessen Daten geladen:
Durch die Navigation im `Seitenmenmenü` haben Sie nun die Möglichkeit die Patientendaten auf
ihre Richtigkeit zu prüfen und allenfalls durch Klicken auf den Schieberegler
`Daten bearbeiten` zu ändern.

![Patient ausgewählt](./doc/img/annotated/Experte_Patient_ausgewaehlt.png)\

Haben Sie alle relevanten Informationen geprüft, können Sie durch Klicken auf
den `Haken Button` auf dem Bildschrim unten rechts die Daten als
`Verifiziert` markieren (Diese Funktion ist während des Bearbeitens der Daten nicht verfügbar). Beim Patienten erscheint nun in der App ein grüner
Haken als Bestätigung dafür, dass seine Daten geprüft wurden.

Bearbeitet der Patient seine Daten nachdem Sie diese geprüft haben,
verschwindet diese Bestätigung aus seiner App und die Daten müssen erneut
verifiziert werden.
